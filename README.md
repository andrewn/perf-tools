# perf-tools

This project contains simple convenience wrappers around [Brendan Gregg's Flamegraph tools](http://www.brendangregg.com/flamegraphs.html) and the BCC Tools for BPF-based Linux IO analysis. It is not currently intended to be used by a wider audience then myself.

## Install

```bash
$ git clone --recurse-submodules https://gitlab.com/andrewn/perf-tools.git
$ cd perf-tools
$ sudo ./bin/install-ubuntu.sh
```

## System-wide CPU Flamegraphs

```bash
$ sudo bin/system-flamegraph.sh
~/perf-tools ~/perf-tools
# Recording 60 second on-cpu flamegraph
[ perf record: Woken up 38 times to write data ]
[ perf record: Captured and wrote 13.164 MB perf.data (95040 samples) ]
# SVG saved to /tmp/tmp.irIChwTNiZ-perftools-system-cpu-flamegraph.svg.gz
# Retrieve it with 'scp web-01:/tmp/tmp.irIChwTNiZ-perftools-system-cpu-flamegraph.svg .'
```

## Gitaly IO Snooping

`gitalysnoop` is a tool for discovering IO utilisation for a Gitaly host.

It uses eBPF to monitor the processes spawned from a Git process, as well as their children, and tracks block IO operations for these processes.

On an interval (specified by the `wait` parameter) the most expensive repositories and git processes (in terms of IO used by that repository/command) are displayed.

This can be helpful in tracking down noisy repositories.

```bash
$ sudo gitalysnoop --wait 10 --max-repos 10
REPO STATS
key                                                                                                                             count    block_reads   block_writes      total_bio
@hashed/72/ce/72ce4b701204d192e2972ab4724ab51a16c75366fdf5de41131d81a7b79f6f85.git                                                  1            333              0            333
@hashed/7f/7c/7f7cddcd3e607e40893a44be29a5a02a9890c41dbfe6360e46cb172d0c21a07e.git                                                 25            219              0            219
@hashed/c4/3d/c43d7f970aa5b9855bbd8934aeadb49715dd09f74d8bbeccd81b1ad94e546156.git                                                  3            154              0            154
@hashed/0b/aa/0baa0ca725eea54d9a028d605bbbbeca41e1d049767b6c73857edec96d7304d3.git                                                 17             81             57            138
@hashed/8c/32/8c329bee8af0c46729c3550f528b4a685f60ffb95c43071e1156b1b6b5796ece.git                                                  1            124              0            124
@hashed/55/dd/55dd9901243087ef315e97fd277745f76640a5ee6b926dbbec4bab353c06f793.git                                                 31            114              7            121
@hashed/4e/72/4e724c56d0b6650f26f8e74410e4f4ea0a3f73892e40b4d1425441a69e922c5e.git                                                  3            112              0            112
@hashed/a8/83/a883a9b515115af58ad5732f1ec48d55ff54dd33438ee1120b9b3a1a66e13ac9.git                                                  2            112              0            112
@hashed/0b/c2/0bc29b065000d42dc53b9b753d3c6d8cd02e2bb221fe0322a1c7a6372bce4fd6.git                                                  3            109              0            109
@hashed/cd/fc/cdfc97887aabc2f18b0d67528c0d0fa6d8a5f754b6607340f6af2393fcb8dc43.git                                                  2            109              0            109
-------------------------------------------------------------------------------------------------------
COMMAND STATS
key                                                                                                                             count    block_reads   block_writes      total_bio
/opt/gitlab/embedded/bin/git --git-dir <path> cat-file --batch-check                                                              542           1603              0           1603
/opt/gitlab/embedded/bin/git --git-dir <path> for-each-ref --format %(objectname) %(objecttype) %(refname:lstrip=2) refs..         67            517              0            517
/opt/gitlab/embedded/bin/git -c core.hooksPath=/opt/gitlab/embedded/service/gitaly-ruby/git-hooks -c core.alternateRefsC..         14            367            126            493
/opt/gitlab/embedded/bin/git -c receive.maxInputSize=<sha> upload-pack <path>                                                     146            467              0            467
/usr/bin/du -sk <path>                                                                                                             13            391              0            391
/opt/gitlab/embedded/bin/git upload-pack --stateless-rpc --advertise-refs <path>                                                   18            289              0            289
/opt/gitlab/embedded/bin/git --git-dir <path> log --format=%H --max-count=1 <sha> --                                              172            195              0            195
/opt/gitlab/embedded/bin/git --git-dir <path> log --format=format:%H --max-count=1 master --                                        5            179              0            179
/opt/gitlab/embedded/bin/git --git-dir <path> for-each-ref --format=%(refname)%00%(objectname) --sort=refname refs/heads..         45            115              0            115
/opt/gitlab/embedded/bin/git --git-dir <path> update-ref -z --stdin                                                                27            103              0            103
-------------------------------------------------------------------------------------------------------
MOST EXPENSIVE COMMANDS
key                                                                                                                             count    block_reads   block_writes      total_bio
/opt/gitlab/embedded/bin/git -c receive.maxInputSize=10485760000 upload-pack /var/opt/gitlab/git-data/repositories/@hashed/72/ce/72ce4b701204d192e2972ab4724ab51a16c75366fdf5de41131d81a7b79f6f85.git           1            333              0            333
/usr/bin/du -sk /var/opt/gitlab/git-data/repositories/@hashed/55/dd/55dd9901243087ef315e97fd277745f76640a5ee6b926dbbec4bab353c06f793.git           1            166              0            166
/opt/gitlab/embedded/bin/git --git-dir /var/opt/gitlab/git-data/repositories/@hashed/c4/3d/c43d7f970aa5b9855bbd8934aeadb49715dd09f74d8bbeccd81b1ad94e546156.git cat-file --batch-check           1            149              0            149
/opt/gitlab/embedded/bin/git --git-dir /var/opt/gitlab/git-data/repositories/@hashed/7f/7c/7f7cddcd3e607e40893a44be29a5a02a9890c41dbfe6360e46cb172d0c21a07e.git log --format=format:%H --max-count=1 master -- README.md           1            136              0            136
/opt/gitlab/embedded/bin/git -c core.hooksPath=/opt/gitlab/embedded/service/gitaly-ruby/git-hooks -c core.alternateRefsCommand=exit 0 # -c receive.maxInputSize=10485760000 receive-pack /var/opt/gitlab/git-data/repositories/@hashed/0b/aa/0baa0ca725eea54d9a028d605bbbbeca41e1d049767b6c73857edec96d7304d3.git           1             79             57            136
/opt/gitlab/embedded/bin/git upload-pack --stateless-rpc --advertise-refs /var/opt/gitlab/git-data/repositories/@hashed/8c/32/8c329bee8af0c46729c3550f528b4a685f60ffb95c43071e1156b1b6b5796ece.git           1            124              0            124
/opt/gitlab/embedded/bin/git -c core.hooksPath=/opt/gitlab/embedded/service/gitaly-ruby/git-hooks -c core.alternateRefsCommand=exit 0 # -c receive.maxInputSize=10485760000 receive-pack --stateless-rpc /var/opt/gitlab/git-data/repositories/@hashed/55/dd/55dd9901243087ef315e97fd277745f76640a5ee6b926dbbec4bab353c06f793.git           1            114              7            121
/usr/bin/du -sk /var/opt/gitlab/git-data/repositories/@hashed/62/00/6200ae43d6ca9ed1e68b658857aec2273011d6b4fd22d93b0f20007e971f206b.git           1            121              0            121
/opt/gitlab/embedded/bin/git --git-dir /var/opt/gitlab/git-data/repositories/@hashed/a8/83/a883a9b515115af58ad5732f1ec48d55ff54dd33438ee1120b9b3a1a66e13ac9.git cat-file --batch-check           1            110              0            110
/opt/gitlab/embedded/bin/git --git-dir /var/opt/gitlab/git-data/repositories/@hashed/4e/72/4e724c56d0b6650f26f8e74410e4f4ea0a3f73892e40b4d1425441a69e922c5e.git cat-file --batch-check           1            109              0            109
```
